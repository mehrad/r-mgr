#!/bin/env bash

## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##
##                 ▄▄▄ ▄▄        ▄▄ ▄▄ ▄▄     ▄▄▄ ▄ ▄▄▄ ▄▄
##                  ██▀ ▀▀        ██ ██ ██   ██ ██   ██▀ ▀▀
##                  ██      ████  ██ ██ ██    █▀▀    ██
##                 ▄██▄          ▄██ ██ ██▄  ▀████▄ ▄██▄
##                                          ▄█▄▄▄▄▀
##
##                         [ R version manager ]
##
##
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## Description:
##   The idea of this script is to allow user to safely install and handle
##   multiple R versions on the same Linux machine and easily switch between
##   them.
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## Author:
##   Mehrad Mahmoudian (mehrad@mahmoudian.me)
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## Repo:
##   https://codeberg.org/mehrad/r-mgr
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## Usage:
##   Try running the script with the "help" subcommand
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## License:
##   BSD 3-Clause License
##   
##   Copyright (c) 2023, Mehrad Mahmoudian
##   All rights reserved.
##   
##   Redistribution and use in source and binary forms, with or without
##   modification, are permitted provided that the following conditions are met:
##   
##   * Redistributions of source code must retain the above copyright notice, this
##     list of conditions and the following disclaimer.
##   
##   * Redistributions in binary form must reproduce the above copyright notice,
##     this list of conditions and the following disclaimer in the documentation
##     and/or other materials provided with the distribution.
##   
##   * Neither the name of the copyright holder nor the names of its
##     contributors may be used to endorse or promote products derived from
##     this software without specific prior written permission.
##   
##   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
##   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
##   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
##   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
##   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
##   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
##   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
##   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


#-------[ initial settings ]-------#
## remember to end folder paths with /
{
    # the full path to the locations that R versions are installed
    init_installation_path='/opt/R/'
    
    # the full path to the file/symlink that defines the default version of R
    init_default_read_path='/usr/bin/R'
    
    # the name of tmux session for when the code creates one(e.g during installation of a new version)
    init_tmux_session_name='Rinstallation'
    
    # define some colors for different messages
    init_color_defult="$(tput sgr0)"
    init_color_error="$(tput setaf 1)"
    init_color_success="$(tput setaf 2)"
    init_color_message="$(tput setaf 3)"
    
    # define the path that the R source file should be downloaded from
    #init_R_source_repo_path='https://cloud.r-project.org/src/base/R-3/'
    init_R_source_repo_path='https://cloud.r-project.org/src/base/R-4/'
    
    # define a path to the tmp folder. This will be used to store temprorary files
    init_tmp_path='/tmp/'
}


#-------[ initial setup ]-------#
{
    # stop the script on errors
    set -e
    
    # stop the script on unset variables
    set -u
    
    # stop the script on failed pipes
    set -o pipefail
}


#-------[ internal functions ]-------#
{
    function func_manual () {
        ## Description:
        ##  This function shows the help/manual of this file
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  --nothing--
        
        echo '----------------------------------------------------------------------'
        echo '                 ▄▄▄ ▄▄        ▄▄ ▄▄ ▄▄     ▄▄▄ ▄ ▄▄▄ ▄▄'
        echo '                  ██▀ ▀▀        ██ ██ ██   ██ ██   ██▀ ▀▀'
        echo '                  ██      ████  ██ ██ ██    █▀▀    ██'
        echo '                 ▄██▄          ▄██ ██ ██▄  ▀████▄ ▄██▄'
        echo '                                          ▄█▄▄▄▄▀'
        echo
        echo '                         [ R version manager ]'
        echo '----------------------------------------------------------------------'
        echo 'Description:'
        echo '    An easy-to-use software to install and manage the installed R'
        echo '     version, and setting the default version.'
        echo
        echo 'Usage:'
        echo '  r-mgr [-v | --version] [-h | --help] <subcommand> [<args>]'
        echo
        echo 'Subcommands:'
        echo '  At least and at most one of the following subcommands should be'
        echo '   provided.'
        echo
        echo '   change                To change the defult R version'
        echo '   check                 To check what is the default version at the'
        echo '                          moment'
        echo '   l, list               To list all the installed versions'
        echo '   i, install            To install a new version'
        echo '   remove                To remove (uninstall) a version'
        echo '   h, help, --help       Show this help'
        echo '   -v, --version         Show version number'
        echo
        echo 'args:'
        echo '   [version number]      The R version number. Currently only for the'
        echo '                          "change" and "install" subcommands. If'
        echo '                          version number is provided as argument, it'
        echo '                          tries to match that to available versions.'
        echo '                          If no argument is provided, it will list the'
        echo '                          available versions and will let the user to'
        echo '                          choose the version interactively through a'
        echo '                          menu.'
        echo
        echo 'Examples:'
        echo '   # list installed versions'
        echo '   $ r-mgr list'
        echo
        echo '   # install a new version and select the version interactively'
        echo '   $ r-mgr install'
        echo
        echo '   # install a new version directly'
        echo '   $ r-mgr install 4.4.0'
        echo 
        echo '----------------------------------------------------------------------'
        echo ' For more info visit: https://codeberg.org/mehrad/r-mgr'
        echo '----------------------------------------------------------------------'
    }
    
    
    function func_msg () {
        ## Description:
        ##  It produce color-coded and indented text to the user of the code
        ##
        ## Arguments:
        ##  - first argument:  is the number of indentation. Think about it as
        ##                      the level number you want the message to be
        ##                      shown.
        ##  - second argument: defines the color of the shown message.
        ##  - third argument:  the string you want to show.
        ##
        ## Output:
        ##  --nothing--
        
        case "${2}" in
            m|message)
                color=${init_color_message}
                ;;
            e|error)
                color=${init_color_error}
                ;;
            s|success)
                color=${init_color_success}
                ;;
            *)
                color=${init_color_defult}
                ;;
        esac
        
        times=${1}
        
        ## create as manu indentation as the user have asked for
        #indent=$(for i in {1..${times}}; do echo -n '|_ '; done)
        #indent="$(yes '|_ ' | head -n ${times})"
        #indent=$(printf "%${times}s" | sed 's/ /|_ /g')
        indent=$(for (( c=1; c<${times}; c++)) ; do echo -n "|  " ; done)
        
        #echo -e "|_ ${color}${3}${init_color_defult}"
        echo -e "${indent}|_ ${color}${3}${init_color_defult}"
    }
    
    
    function func_choice_of () {
        ## Description:
        ##  This function reads a list of items as input, ask the user to
        ##   choose and set a global variable names selected_item with the
        ##   choice of the user.
        ##
        ## Arguments:
        ##  A series of items to be asked form user
        ##
        ## Output:
        ##  Does not return anything, but instead it sets a global variable
        ##  named selected_item with the item string that the user have chosen
        
        selected_item='NULL'
        
        select selected_item
        # [in list] omitted, so 'select' uses arguments passed to function.
        do
            if [ -n "${selected_item}" ]; then
                func_msg 2 message "You chose: ${selected_item}"
                break
            else
                func_msg 2 error 'Not a valid choice. You should type the number on the left side of what you want to choose.'
            fi
        done
    }
    
    
    function func_change_version () {
        ## Description:
        ##  This function reads a list installed versions, asks the user what
        ##  they want to set as default and will set that by symlinking the
        ##  /usr/bin/R
        ##
        ## Arguments:
        ##  A series of items to be asked form user (optional)
        ##
        ## Output:
        ##  Does not return anything
        
        #-------[ check the requirements ]-------#
        {
            # make sure the user has sudo access
            if [ "$(func_has_sudo)" = 'no_sudo' ]; then
                func_msg 1 error 'You need to have sudo access to install R'
                echo
                exit 1
            fi
            
            
            # make sure the init_installation_path is a directory and exists
            if [ ! -d "${init_installation_path}" ]; then
                func_msg 1 error "The folder ${init_installation_path} that is suppose to contain installed versions of R does not exist!\n   Try using \"install\" argument to install the desired version of R through this script."
                echo
                exit 1
            fi
            
            
            # make sure that some R version are already installed
            if [ $(ls -1 "${init_installation_path}" | wc -l) -eq 0 ]; then
                func_msg 1 error "No R version have been installed in ${init_installation_path} \n   Use the \"install\" argument to install your desired R version."
                echo
                exit 1
            fi
            
            
            # make sure the init_default_read_path is a symlink
            # if the file exists but it is not a symlink
            if [ ! -L "${init_default_read_path}" ] && [ -f "${init_default_read_path}" ]; then
                func_msg 1 error "The file ${init_default_read_path} is not a symlink!\n   Perhaps you have already installed R by other means.\n   Remove that and try installing through this script!"
                echo
                exit 1
            fi
        }
        
        # capture the argument
        local args
        args=$@

        local new_default_version
        
        # if no argument was passed to this function
        if [ -z "${args}" ]; then
            # get the list of installed R versions
            installed_versions=$(ls ${init_installation_path})
            
            # ask the user what version they want to set as default
            func_msg 1 warning 'Select the R version which you want to be the default on your system by entering the index number:'
            func_choice_of ${installed_versions}
            
            new_default_version="${selected_item}"
        else
            new_default_version="${args}"
        fi
        
        echo '|'
        func_msg 1 warning 'The following settings will be applied:'
        func_msg 2 warning "${init_default_read_path} ----> $(func_path_cleaner "${init_installation_path}${new_default_version}/bin/R")"
        func_msg 2 warning "${init_default_read_path}script ----> $(func_path_cleaner "${init_installation_path}${new_default_version}/bin/Rscript")"
        echo '|'
        
        # set the selected version as the default in form of a symlink
        sudo ln -sf  "$(func_path_cleaner "${init_installation_path}${new_default_version}/bin/R")" ${init_default_read_path}
        sudo ln -sf  "$(func_path_cleaner "${init_installation_path}${new_default_version}/bin/Rscript")" ${init_default_read_path}script
        
        
        # check if the change was successful
        if [ "$(func_R_symlink_health)" = 'healthy' ]; then
            # show the user the final symlink status
            func_check_status
        else
            func_msg 1 error 'We failed to set the default R version!'
        fi
    }
    
    
    function func_check_status () {
        ## Description:
        ##  It checks where the default R path is linked to and return a human
        ##   readable string
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  it just returns a string value

        local symlink_status
        symlink_status="$(func_R_symlink_health)"
        
        case "${symlink_status}" in
        file_does_not_exists)
            func_msg 1 error "There is no file named '${init_default_read_path}'\n   Perhaps you have not ever set the R version.\n   Try using the \"change\" argument to set the version.";
            echo;
            exit 1;
            ;;
        file_not_symlink)
            func_msg 1 error "The file '${init_default_read_path}' is not a symlink!\n   Perhaps you have already installed R by other means.\n   Remove that and try installing through this script!";
            echo;
            exit 1;
            ;;
        symlink_broken)
            func_msg 1 error "The link of the '${init_default_read_path}' is broken!\n   Try using the \"change\" argument to set the version and fix this.";
            echo;
            exit 1;
            ;;
        healthy)
            #local R_symlink="${init_default_read_path} ---> $(readlink -f ${init_default_read_path})"
            #echo "${R_symlink}"
            func_msg 1 success "${init_default_read_path} ---> $(readlink -f ${init_default_read_path})";
            ;;
        *)
            func_msg 1 error 'Please report this: Unknown condition in func_check_status!';
        esac
    }
    
    
    function func_package_manager () {
        ## Description:
        ## a function to detect the package manager of the linux distro
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  it just returns a string value containing the type of the package manager
        
        local pakacgemanager
        pakacgemanager='unknown'

        if hash apt 2>/dev/null; then
            # make sure this is the apt package manager
            if apt --help | grep --fixed-strings "apt.conf(5)" &> /dev/null; then
                pakacgemanager='apt'
            fi
        fi

        if hash pacman 2>/dev/null; then
            # make sure this is the pacman package manager and not another pacman like the game in Ubuntu!
            if pacman --version | grep "libalpm" &> /dev/null; then
                pakacgemanager='pacman'
            fi
        fi
        
        # return the type of package manager
        echo "${pakacgemanager}"
    }
    
    
    function func_install () {
        ## Description:
        ##  if called without argument, it will fetch the available versions
        ##   from the server which is specified at ${init_R_source_repo_path}.
        ##   If the version number of provided, it will try to match the
        ##   provided version with the possible versions from the server and if
        ##   matched, install it.
        ##
        ## Arguments:
        ##  Optional string of version number to be installed
        ##
        ## Output:
        ##  
        
        # get all available R versions based on the defined repo URL
        #available_versions=$(curl --silent ${init_R_source_repo_path} | grep 'href="R' | egrep -o 'R-[0-9](.[0-9])+')
        
        # store the current path to be returned to after installation
        before_installation_path="$(pwd)"
        
        
        #-------[ check requirements and fix them ]-------#
        {
            # make sure the user has sudo access
            if [ "$(func_has_sudo)" = 'no_sudo' ]; then
                func_msg 1 error 'You need to have sudo access to install R'
                echo
                exit 1
            fi
            
            
            # make sure the init_installation_path is a directory and exists
            if [ ! -d "${init_installation_path}" ]; then
                func_msg 1 error "The folder '${init_installation_path}' that is suppose to contain installed versions of R does not exist!\n   Should we create it?"
                func_choice_of "Yes" "No"
                if [ "${selected_item}" = 'No' ]; then
                    func_msg 1 error 'We cannot proceed with installing R since the the defined path for installation does not exists, and script is not authorized to create it either.\n   Please either create it yourself or change the defined path in the begining of the script under the variable name `init_installation_path`.'
                    exit 1
                fi

                func_msg 2 warning "Creating folder '${init_installation_path}'"
                sudo mkdir "${init_installation_path}"
                if [ ! -d "${init_installation_path}" ]; then
                    func_msg 2 error "Failed to create the installation directory: '${init_installation_path}'"
                fi
            fi
            
            
            # make sure the libreadline is installed
            if [ ! -f '/usr/local/lib/libreadline.so' ] && \
               [ ! -f '/usr/lib/x86_64-linux-gnu/libreadline.so' ]; then
                
                func_msg 1 warning 'The libreadline.so was not found. Should we attempt to install it?'
                func_choice_of 'Yes' 'No'
                if [ "${selected_item}" = 'No' ]; then
                    func_msg 1 error 'We cannot proceed with installing R since this is a dependency. Please install it yourself.'
                    exit 1
                fi
               
                # change the directory to the download tmp folder
                cd "${init_tmp_path}"
                
                # get the name of the latest readline version
                latest_readline_version=$(curl --silent 'ftp://ftp.gnu.org/gnu/readline/' | \
                                          grep "readline-[1-9].*.tar.gz$" | \
                                          awk '{print $9}' | \
                                          sort | \
                                          tail -n 1)
                
                func_msg 1 warning "We are going to install ${latest_readline_version}"
                
                # download the latest readline version
                wget -c -N "https://ftp.gnu.org/gnu/readline/${latest_readline_version}"
                
                # extract the downloaded file
                tar -xzf "${latest_readline_version}"
                
                # strip the file extension to get to the folder name that the source files have extracted to
                extracted_path="$(echo "${latest_readline_version}" | awk -F '.tar' '{print $1}')"
                
                cd "${extracted_path}/"
                
                # Build
                ./configure
                make
                sudo make install
                
                # safety check
                if [ ! -f '/usr/local/lib/libreadline.so' ] && \
                   [ ! -f '/usr/lib/x86_64-linux-gnu/libreadline.so' ]; then
                    func_msg 1 error 'Failed to install the readline. we are already in the downloaded location for you to investigate this further.'
                    exit 1
               else
                    func_msg 1 success 'extracted_path installed successfully.'
               fi
                
                # return to path before installation
                cd "${before_installation_path}"
            fi
            
            
            # make sure LaTeX is installed (the installation of packages would fail since documentationsc cannot be parsed to PDF)
            if [ ! $(command -v pdflatex) ]; then
                func_msg 1 error 'LaTeX is not installed and it is needed for compiling R documentations.'
                func_msg 2 warning 'Try installing LaTeX via'
                
                if [ "$(func_package_manager)" = 'apt' ]; then
                    func_msg 3 warning 'sudo apt update ; sudo apt install texlive-full libpcre2-dev'
                elif [ "$(func_package_manager)" = 'pacman' ]; then
                    func_msg 3 warning 'sudo pacman --refresh --refresh ; sudo pacman -S texlive-most'
                fi
                
                # exit the script
                exit 1
            fi
        }
        
        
        # get available R versions on the cloud
        available_versions=$(curl --silent "${init_R_source_repo_path}" \
                             | grep 'href="R' \
                             | gawk '{
                                       gsub(/(<[^>]*>)|(\.tar\.gz)|(R-)/, "");
                                       gsub(/[[:space:]]{2,}/, " ");
                                       printf "%s_(%s)\n", $1, $2
                                     }')

        
        # A flag to indicate if we should still as user the versions in a selection menu
        local flag_let_user_choose
        flag_let_user_choose=1
        
        # if user have requested a specific version via argument
        if [ "$#" -gt 0 ]; then
            local number_of_matched_versions
            number_of_matched_versions="$(echo "$available_versions" \
                                          | awk -v requested_version="^${1}_" '$0 ~ requested_version' \
                                          | wc -l)"
            flag_let_user_choose=0
            
            # if there was only and only one match, use that as the selection
            if [ "${number_of_matched_versions}" -eq 1 ]; then
                selected_item="$(echo "$available_versions" \
                                 | awk -v requested_version="^${1}_" '$0 ~ requested_version')"
            
            # if there were more than one match
            elif [ "${number_of_matched_versions}" -gt 1 ]; then
                func_msg 1 error 'The requested version matches more than one version and therefore is invalid! Do you want to choose from a list?'
                func_choice_of 'Yes' 'No'
                if [ "${selected_item}" == 'No' ]; then
                    exit 1
                else
                    flag_let_user_choose=1
                fi
            
            # if there were zero match
            elif [ "${number_of_matched_versions}" -eq 0 ]; then
                func_msg 1 error 'The requested version does not match any of the available versions. Do you want to choose from a list?'
                func_choice_of 'Yes' 'No'
                if [ "${selected_item}" == 'No' ]; then
                    exit 1
                else
                    flag_let_user_choose=1
                fi
            fi
        fi
        
        
        # if we want to present to user a list of versions to choose from
        if [ "${flag_let_user_choose}" -eq 1 ]; then
            # ask user which one to be installed (the output will be in ${selected_item} variable
            func_msg 1 warning 'Select the R version you wish to install by typing its index number:'
            func_choice_of ${available_versions}
        fi

        # this variable will contain the version (only version) of R by the user
        local selected_version
        # extract the version number of selected R version
        selected_version="$(echo "${selected_item}" | awk -F '_' '{print $1}')"
        
        # the file that we should download
        local selected_file
        selected_file="R-${selected_version}.tar.gz"
        
        # construct the folder name that the source files will be extracted to
        local extracted_R_source_path
        extracted_R_source_path="R-${selected_version}"
        
        if [ -d "${init_installation_path}/${selected_version}" ]; then
            func_msg 1 warning "The selected version is already installed at '$(func_path_cleaner "${init_installation_path}/${selected_version}")'"
            exit 1
        fi
        
        
        echo '|'
        func_msg 1 warning "Downloading '${selected_file}' source file."
        
        # downlaod the file
        curl "${init_R_source_repo_path}/${selected_file}" --output "${init_tmp_path}/${selected_file}"
        
        
        # make sure the installation path exist
        sudo mkdir -p "${init_installation_path}"
        
        # change the directory to the download tmp folder
        cd "${init_tmp_path}"
        
        # extract the downloaded file
        tar --extract --gzip --file="${selected_file}" --directory="${init_tmp_path}"
        
        func_msg 1 success "The source was downloaded and extracted in '$(func_path_cleaner "${init_tmp_path}/${extracted_R_source_path}")'"
        
        # go inside the extracted folder
        cd "${extracted_R_source_path}"
        
        func_msg 1 warning 'Installing dependencies for building the source'
        
        
        if [ "$(func_package_manager)" = 'apt' ]; then
        
            # check if the sources.list file has any source lines that is commented out
            if [ $(cat '/etc/apt/sources.list' | grep '^# deb-src' | wc -l) -gt 0 ]; then
                func_msg 1 warning 'There are some source lines in "/etc/apt/sources.list" that are commented. We will make a backup copy ("/etc/apt/sources.list.bk") and try to fix the original file.'
                
                # create a bckup from the sources.list file
                sudo cp '/etc/apt/sources.list' '/etc/apt/sources.list.bk'
                
                func_msg 2 success 'Backup was created.'
                
                # remove the comments from all the source lines
                sudo sed -Ei 's/^# deb-src /deb-src /' '/etc/apt/sources.list'
                
                func_msg 2 success 'Lines were uncommented.'
            fi
            
            
            # get the dependencies
            sudo apt -qq update
            sudo apt -qq build-dep r-base
            
        elif [ "$(func_package_manager)" = 'pacman' ]; then
            # get the dependencies and only install the packages that are not up-to-date or not installed
            sudo pacman --sync \
                        --needed \
                        $(pacman --sync --info r | awk -F ":" '/Depends On/ { print $2}')
            
        fi
        
        
        func_msg 1 warning 'Building from source'
        
        # build the R from source ( https://docs.posit.co/resources/install-r-source/ )
        ./configure --enable-jit \
                    --prefix="$(func_path_cleaner "${init_installation_path}/${selected_version}")" \
                    --enable-R-shlib \
                    --enable-memory-profiling \
                    --with-blas \
                    --with-lapack \
                    --with-x=yes
        make -j$(nproc --ignore 1)
        sudo make install
        sudo make install-info
        sudo make install-pdf

        echo '--------------------------------------------------------------------------------'
        func_msg 1 success 'Successfully installed!'
        
        # return to path before installation
        cd "${before_installation_path}"
        
        func_msg 1 warning "Do you want this installed version (${selected_version}) to be the default?"
        func_choice_of 'Yes' 'No'
        if [ "${selected_item}" = 'Yes' ]; then
            func_change_version "${selected_version}"
        fi
        
        # clean up
        #rm -rf "${init_tmp_path}${selected_item}"
    }
    
    
    function func_uninstall () {
        
        #-------[ check requirements and fix them ]-------#
        {
            # make sure the user has sudo access
            if [ "$(func_has_sudo)" = 'no_sudo' ]; then
                func_msg 1 error 'You need to have sudo access to remove an installed version of R'
                echo
                exit 1
            fi
        }
        
        local installed_versions
        installed_versions=$(func_list_installed)
        
        func_msg 1 warning 'Which one these versions should be removed?'
        func_choice_of ${installed_versions}
        
        local to_be_removed
        to_be_removed="${selected_item}"
        
        echo '|'
        func_msg 1 warning "Are you sure you want to remove R version '${to_be_removed}'?"
        func_choice_of 'Yes' 'No'
        if [ "${selected_item}" = 'No' ]; then
            func_msg 1 warning "Removal of the R version '${to_be_removed}' is aborted!"
            exit 1
        elif [ "${selected_item}" = 'Yes' ]; then
            sudo rm -rf "${init_installation_path}/${to_be_removed}"
            
            if [ -d "${init_installation_path}/${to_be_removed}" ]; then
                func_msg 1 error 'Failed to removed the selected R version!'
                exit 1
            fi
            
            func_msg 1 success 'Removed!'
            
            # check if the symlink link is broken (perhaps it was linked to this version that we removed), suggest user to "change" the version
            if [ "$(func_R_symlink_health)" = 'symlink_broken' ]; then
                func_msg 1 warning 'There is no working default R version. Do you want to set a working default?'
                func_choice_of 'Yes' 'No'
                if [ "${selected_item}" = 'Yes' ]; then
                    func_change_version
                fi
            fi
        fi
    }
    
    
    function func_has_sudo () {
        ## Description:
        ##  a function to check if the user has sudo access and if he has, does it need password
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  it just returns a sting value
        ##
        ## source:
        ##   https://superuser.com/a/1281228
        
        local prompt
        
        prompt="$(sudo --non-interactive --validate 2>&1)"
        if [ $? -eq 0 ]; then
            echo 'has_sudo__pass_set'
        elif echo "${prompt}" | grep --quiet '^sudo:'; then
            echo 'has_sudo__needs_pass'
        else
            echo 'no_sudo'
        fi
    }
    
    
    function func_R_symlink_health () {
        ## Description:
        ##  a function to check if the file in init_default_read_path is healthy symlink
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  it just returns a sting value of the status
        
        if [ ! -f "${init_default_read_path}" ]; then
            echo 'file_does_not_exists'
        elif [ ! -L "${init_default_read_path}" ]; then
            echo 'file_not_symlink'
        elif [ ! -e "${init_default_read_path}" ]; then
            echo 'symlink_broken'
        else
            echo 'healthy'
        fi
    }
    
    
    function func_list_installed () {
        ## Description:
        ##  a function to list all the installed versions
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  it just returns a sting value of the status
        
        local installed_versions
        installed_versions="$(ls -1 ${init_installation_path})"
        echo "${installed_versions}"
    }


    function func_list () {
        ## Description:
        ##  a function to do what the "list" subcommand is suppose to do
        ##
        ## Arguments:
        ##  --nothing--
        ##
        ## Output:
        ##  Prints the results to screen
        
        func_msg 1 success "The following versions are installed in '${init_installation_path}' :";

        local tmp_current_version
        local tmp_symlink_status
        tmp_symlink_status="$(func_R_symlink_health)"

        # make sure the file in init_default_read_path is a healthy symlink
        if [ "${tmp_symlink_status}" = 'healthy' ]; then
            tmp_current_version="$(readlink -f ${init_default_read_path} | grep -o -P "(?<=${init_installation_path}).+(?=/bin/R)")"
        fi
        
        # for every installed version
        for i in $(func_list_installed); do
            if [ "${i}" = "${tmp_current_version}" ]; then
                i="${i} <-- current active version";
            fi;
            func_msg 2 message "${i}";
        done;
        
        if [ "${tmp_symlink_status}" = 'file_not_symlink' ]; then
            func_msg 1 error "The file in '${init_default_read_path}' is not a symlink. Perhaps you have installed R by some approach other than r-mgr. Please consider uninstalling R and installing it again using r-mgr."
            
        elif [ "${tmp_symlink_status}" = 'symlink_broken' ]; then
            func_msg 1 error "The file in '${init_default_read_path}' is a broken symlink! try running 'unlink ${init_default_read_path} && r-mgr change' to create it correctly and set the R version you want."
            
        elif [ "${tmp_symlink_status}" = 'healty' ]; then
            echo
            func_msg 1 error "The file in '${init_default_read_path}' is weird! Please report the following to:"
            func_msg 2 message "https://codeberg.org/mehrad/r-mgr/issues"
            echo
            echo '----------------------------- report the following -----------------------------'
            ls -alh "${init_default_read_path}"
            echo
            echo "Status: ${tmp_symlink_status}"
            echo '------------------------------- report the above -------------------------------'
        fi
    }
    
    
    function func_path_cleaner () {
        ## Description:
        ##  a function to remove extra / from given string
        ##
        ## Arguments:
        ##  one string to be cleaned
        ##
        ## Output:
        ##  returns a sting value
        
        echo "${1}" | sed --regexp-extended --expression='s_/{2,}_/_'
    }
}


#-------[ checking the input ]-------#
{
    # check if arguments are provided
    if [ "$#" -eq '0' ]; then
        func_msg 1 error 'No arguments supplied'
        echo
        func_manual
        exit 1
    fi
}


#------[ parsing arguments ]-------#
{
    case "${1}" in
        change)
            echo
            if [ "$#" -gt '1' ]; then
                func_change_version "${2}";
            else
                func_change_version;
            fi
            ;;
        check)
            echo
            func_check_status;
            ;;
        l|list)
            echo
            func_list;
            ;;
        i|install)
            echo
            if [ "$#" -gt '1' ]; then
                func_install "${2}";
            else
                func_install;
            fi
            ;;
        remove)
            echo
            func_uninstall;
            ;;
        h|help|-h|--help)
            func_manual;
            ;;
        version|-v|--version)
            echo "1.1.1";
            exit 0;
            ;;
        *)
            func_manual;
            echo;
            exit 1;
            ;;
     esac
     echo
}

