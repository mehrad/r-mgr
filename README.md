# r-mgr: Easy to use R version manager

A simple and easy-to-use tool for managing R versions on a Linux machine. This tool allow the user to install multiple R versions on the same Linux machine and switch between them at will.


**Table of Content**
- [Supported Linux distros](#supported-linux-distros)
- [Installation](#installation)
- [Usage](#usage)
- [Contribute](#contribute)
- [Clarification](#clarification)

![R.version.manager demo](./assets/r-mgr_intro.gif)

## Supported Linux distros

Although the scripts can be run on any distro, some of the scripts have distro-specific parts, for example for getting the list of distro-specific dependency lists. Therefore some of the distros are the intended environment for the scripts:

- `pacman`-based distros, e.g. Arch, Manjaro, EndeavourOS
- `apt`-based distros, e.g Ubuntu, Mint, Elementary OS, Pop!_OS

--------------------------------------------------------------------------------

## Installation

1. Clone this repo on your computer where ever you like, for example:

```sh
cd ~
git clone --depth=1 https://codeberg.org/mehrad/r-mgr.git
```

You can use the script `r-mgr.sh` if you like to, but if you want to install it as a command on your computer (which I recommend it), you can use:

```sh
# navigate to the cloned folder
cd r-mgr/

# install
make install
```

--------------------------------------------------------------------------------

## Usage

If you have not installed it on your machine, you can navigate to the folder in which you have cloned this repo and run it by:

```sh
bash r-mgr.sh --help
```

If you have installed it, just open a new terminal (not every shell refreshes automatically after installation of a new software).

```sh
r-mgr --help
```

To **list** the installed R versions on your computer, you can do:

```sh
r-mgr list
```

To **install** a particular R version, you can use `r-mgr install` to see the list of available versions, or if you already know which version you want, you can directly provide the version number to the `install` subcommand:

```sh
# if you want to select the version interactively
r-mgr install

# if you already know which version you want
r-mgr install 4.3.1
```

--------------------------------------------------------------------------------

## Contribute

Checklist:

1. Make sure your code is commented and variable names are intuitive enough.
2. please consider abiding by the [EditorConfig](https://editorconfig.org) file that is in this repo.
3. Test and make sure your modification works as expected and has not broken anything else.
4. Create pull request to have your contribution under your name and be acknowledged for them.

You can use the `Makefile` to your advantage. Use it and save your time:

```sh
make help
```

--------------------------------------------------------------------------------

## Clarification

The idea is to support Linux and Linux only. Maybe in future macOS support is added, but do not even think about asking support for Windows! If you are a Windows user and want a quality-of-life improvment, first ditch that awefully limiting OS!

